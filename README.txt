----Set up----
git clone from https://bitbucket.org/andrew_blaney/kihoutei

install django 1.8, python3, pip, virtualenv, virtualenvwrapper			(pip freeze on linux shows packages installed)

make env:			mkvirtualenv -p /Python3Path ki
start virtualenv: 	workon ki
in django path with manage.py python manage.py runserver
go to localhost:8000/admin/
login name: ab
pass: 		ab

browse the gear

----Models----

Curators extend auth.users by OneToOne relationship
Curator model extends Django user auth model
Curators can belong to Tribes

Tribes have a ManyToMany relationship with Curators
Through Membership model

see Curator.tribe_count see https://docs.djangoproject.com/en/1.8/topics/db/models/#many-to-many-relationships

Curator = target
Tribe = intermediate
Membership = through


----Admin----

Memberships are displayed inline under Curators and Tribes

fix admin problems


----Views----

**change to render_to_response 

**update to Generic views / add more info
	-- done but need to update curators to this

----TODO----

**tribes.models.Tribe - Limit number of Tribes Curators can belong to to 3 

Structure *move templates into more reuasable directory ?
Templates very simlar feel like something can be done?

--PROBLEMS--
cant add reg date.





from django.shortcuts import render
from django.views import generic
from .models import Tribe
# Create your views here.

class IndexView(generic.ListView):
    template_name = 'tribes/index.html'
    context_object_name = 'tribe_list'

    def get_queryset(self):
        """Return all Tribes."""
        return Tribe.objects.all()

class DetailView(generic.DetailView):
    model = Tribe
    template_name = 'tribes/detail.html'
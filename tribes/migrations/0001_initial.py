# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Membership',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('date_joined', models.DateTimeField(auto_now_add=True)),
                ('curator', models.ForeignKey(to='curators.Curator')),
            ],
        ),
        migrations.CreateModel(
            name='Tribe',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=60)),
                ('members', models.ManyToManyField(to='curators.Curator', through='tribes.Membership')),
            ],
        ),
        migrations.AddField(
            model_name='membership',
            name='tribe',
            field=models.ForeignKey(to='tribes.Tribe'),
        ),
    ]

from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^tribe/$', views.IndexView.as_view(), name='tribeindex'),
	url(r'^tribe/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='tribedetail'),
]

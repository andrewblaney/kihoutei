from django.db import models
from curators.models import Curator
# Create your models here.

class Tribe(models.Model):
	name = models.CharField(max_length=60)
	members = models.ManyToManyField(Curator, through="Membership")

	def __str__(self):
		name = str(self.name)
		name.title()
		return name

class Membership(models.Model):
	curator = models.ForeignKey(Curator)
	tribe = models.ForeignKey(Tribe)
	date_joined = models.DateTimeField(auto_now_add=True, blank=True)

	def __str__(self):
		return str(self.curator.user.username).title() + " belongs to " + str(self.tribe)  

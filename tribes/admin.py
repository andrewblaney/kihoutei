from django.contrib import admin
from .models import Tribe, Membership
# Register your models here.


class MembershipInline(admin.TabularInline):
	model = Membership
	extra = 0

class TribeAdmin(admin.ModelAdmin):
	inlines = [MembershipInline]

admin.site.register(Tribe, TribeAdmin)
admin.site.register(Membership)



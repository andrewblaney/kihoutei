from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render_to_response
from .models import Curator
# Create your views here.

def home(request):
	get = Curator.objects.order_by('-id')[:1]
	user = get[0]
	return render(request, 'curators/home.html', {"newest": user})

def index(request):
	curator_list = Curator.objects.all()
	return render(request, 'curators/index.html', {"curator_list": curator_list},)


def detail(request, user_id):
	curator = get_object_or_404(Curator, pk=user_id)
	return render(request, 'curators/detail.html', {"curator": curator},)

	


from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.home, name='home'),
	url(r'^curators/$', views.index, name='curatorindex'),
	url(r'^curators/(?P<user_id>[0-9]+)/$', views.detail, name='curatordetail'),
]
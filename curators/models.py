from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Curator(models.Model):
	user = models.OneToOneField(User)
	reg_date = models.DateTimeField('registration date', blank=True, null=True)

	def __str__(self):
		return self.user.username.title()

	def tribe_count(self):
		return self.tribe_set.count()


	tribe_count.short_description = '# of Tribe memberships'
	tribe_count.admin_order_field = 'tribe_count'




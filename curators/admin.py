from django.contrib import admin
from .models import Curator
from tribes.models import Tribe, Membership

# Register your models here.

class MembershipInline(admin.TabularInline):
	model = Membership
	extra = 0

class CuratorAdmin(admin.ModelAdmin):

	inlines = [MembershipInline]
	list_display = ('user', 'tribe_count', 'reg_date')
	
	fieldsets = [
	(None,	{'fields': ['user']}),
	('Other', {'fields': ['reg_date'], 'classes': ['collapse']}),
	]



admin.site.register(Curator, CuratorAdmin)





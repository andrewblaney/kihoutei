# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='curator',
            name='reg_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 7, 13, 37, 16, 229453, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]

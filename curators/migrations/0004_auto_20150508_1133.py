# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0003_auto_20150507_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='curator',
            name='reg_date',
            field=models.DateTimeField(null=True, verbose_name=b'registration date', blank=True),
        ),
    ]

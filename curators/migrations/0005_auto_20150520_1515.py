# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0004_auto_20150508_1133'),
    ]

    operations = [
        migrations.RenameField(
            model_name='curator',
            old_name='user',
            new_name='usermodel',
        ),
    ]

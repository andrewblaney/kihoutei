# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0002_curator_reg_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='curator',
            name='reg_date',
            field=models.DateTimeField(verbose_name=b'registration date', blank=True),
        ),
    ]

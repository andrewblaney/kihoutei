# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0005_auto_20150520_1515'),
    ]

    operations = [
        migrations.RenameField(
            model_name='curator',
            old_name='usermodel',
            new_name='user',
        ),
    ]
